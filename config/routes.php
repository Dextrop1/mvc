<?php

$routes = array(
    array('home','default','index'),

    array('frontpage','admin','index'),
    array('login','security','login'),
    array('register','security','register'),
    array('listing','security','listing'),
    array('single','security','single', array('id')),
    array('edit','security','edit', array('id')),
    array('delete','category','delete', array('id')),

    array('listing-produits','security','listing-produits'),
    array('single-produits','security','single-produits', array('id')),
    array('edit-produits','security','edit-produits', array('id')),
    array('delete-produits','category','delete-produits', array('id')),

);









