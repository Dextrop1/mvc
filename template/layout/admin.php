<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Framework Pédagogique MVC6</title>
    <?php echo $view->add_webpack_style('admin'); ?>
</head>
<body>
<?php // $view->dump($view->getFlash()) ?>
<header style="background-color: black; color: white;" id="masthead">
    <nav>
        <ul style="display: flex;justify-content: space-around;padding: 2rem;     margin: 0;
;
">
            <li style="list-style:none;"><a style="color: wheat;" href="<?= $view->path(''); ?>">Home</a></li>
            <li style="list-style:none;"><a style="color: wheat;" href="<?= $view->path('frontpage'); ?>">Frontpage</a></li>
            <li style="list-style:none;"><a style="color: wheat;" href="<?= $view->path('register'); ?>">register</a></li>
            <li style="list-style:none;"><a style="color: wheat;" href="<?= $view->path('login'); ?>">login</a></li>

        </ul>
    </nav>
</header>
<div class="sidebar">
    <style>
        body {
            margin: 0;
            font-family: Arial, sans-serif;
        }
        .sidebar {
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            background-color: #f2f2f2;
            overflow-x: hidden;
            padding-top: 20px;
        }
        .sidebar ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }
        .sidebar li a {
            display: block;
            color: #000;
            padding: 8px 16px;
            text-decoration: none;
        }

        .sidebar li a:hover {
            background-color: indianred;
        }
        footer{
            text-align: center;
        }
    </style>
    <ul >

        <li style="list-style:none;"><a style="color: #8b6b2e;" href="<?= $view->path(''); ?>">Home</a></li>
        <li style="list-style:none;"><a style="color: #8b6b2e;" href="<?= $view->path('frontpage'); ?>">Frontpage</a></li>
        <li style="list-style:none;"><a style="color: #8b6b2e;" href="<?= $view->path('register'); ?>">register abonné</a></li>
        <li style="list-style:none;"><a style="color: #8b6b2e;" href="<?= $view->path('login'); ?>">login abonné</a></li>
        <li style="list-style:none;"><a style="color: #8b6b2e;" href="<?= $view->path('listing'); ?>">listing abonné</a></li>

    </ul>
</div>
<div class="container">
    <?= $content; ?>
</div>

<footer id="colophon">
    <div style="" class="wrap">
        <p>MVC 6 - Framework Pédagogique.</p>
    </div>
</footer>
<?php echo $view->add_webpack_script('admin'); ?>
</body>
</html>
