<?php

namespace App\Controller;


use App\Model\AdminModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;



/**
 *
 */
class ProduitController extends AbstractController
{
    private $statusList;
    private $v;

    public function __construct()
    {
        $this->v = new Validation();

    }

    public function register()
    {
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $errors = $this->validate($this->v, $post);
            if($this->v->isValid($errors)) {
                AdminModel::insert($post);
                $this->addFlash('success', 'Merci de vous être inscrit !');
                $this->redirect('frontpage');
            }
        }
        $form = new Form($errors);
        $this->render('app.admin.register', array(
            'form' => $form,
        ), 'admin');
    }
    private function getAbonneByIdOr404($id)
    {
        $abonne = AdminModel::findById($id);
        if(empty($abonne)) {
            $this->Abort404();
        }
        return $abonne;
    }


    private function validate($v, $post)
    {
        $errors = array();
        $errors['nom'] = $v->textValid($post['nom'], 'nom', 2, 100);
        $errors['prenom'] = $v->textValid($post['prenom'], 'prenom', 5, 500);
        $errors['email'] = $v->textValid($post['email'], 'email', 5, 250);
        return $errors;
    }
    public function listing() {
        $this->render('app.admin.listing', array(
            'abonnes' => AdminModel::all()
        ), 'admin');
    }
    public function single($id) {
        $abonne = $this->getAbonneByIdOr404($id);
        $this->render('app.admin.single',array(
            'abonne' => $abonne,
        ), 'admin');
    }

    public function edit($id)
    {
        $abonne = $this->getAbonneByIdOr404($id);
        $errors = [];
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors))  {
                AdminModel::update($id,$post);
                $this->addFlash('success', 'ouai ouai edit marche');
                $this->redirect('frontpage');
            }
        }
        $form = new Form($errors);
        $this->render('app.admin.edit', array(
            'form' => $form,
            'abonne' => $abonne,
        ), 'admin');
    }

    public function delete($id) {
        $this->getCategoryByIdOr404($id);
        AdminModel::delete($id);
        $this->addFlash('success', 'Merci ducon pour avoir effacé');
        $this->redirect('frontpage');
    }
}