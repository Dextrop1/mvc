<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ProductModel extends AbstractModel
{
    protected static $table = 'products';

    protected $id;
    protected $titre;
    protected $reference;
    protected $description;
    protected $super;



    public function getSuper()
    {
        return mb_strtoupper($this->nom);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @return mixed
     */
    public function getDescrption()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (titre, reference, description) VALUES (?,?,?)",
            array($post['titre'], $post['reference'], $post['description'],
        );
    }

    public static function update($id,$post)
    {
        App::getDatabase()->prepareInsert(
            "UPDATE " . self::$table . " SET titre = ?, reference = ?, description = ?, WHERE id = ?",
            array($post['titre], $post['reference'], $post['description'],$id)
        );
    }
}
